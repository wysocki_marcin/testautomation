package PageObjects;

import Utils.Utils;
import org.openqa.selenium.By;

public class LoginPage {
    public void typeEmailIntoRegistrationForm(String email) {
        Utils.waitAndFindElement(By.id("email_create")).sendKeys(email);
    }
    public void clickCreateAccount() {
        Utils.waitAndFindElement(By.id("SubmitCreate")).click();
    }

    public void logToApplication(String email, String password) {
        Utils.waitAndFindElement(By.id("email")).sendKeys(email);
        Utils.waitAndFindElement(By.id("passwd")).sendKeys(password);
        Utils.waitAndFindElement(By.id("SubmitLogin")).click();

    }
}
