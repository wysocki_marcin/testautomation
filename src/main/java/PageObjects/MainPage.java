package PageObjects;

import Utils.Utils;
import org.openqa.selenium.By;
import org.testng.Assert;

public class MainPage {

    public void goToCheckout()
    {
        Utils.waitAndFindElement(By.cssSelector(".shopping_cart a")).click();
    }

    public void checkIfCartIsEmpty() {
       int numberOfProducts =  Utils.getDriver().findElements(By.cssSelector(".product_img_link")).size();
       String warning =  Utils.waitAndFindElement(By.cssSelector(".alert-warning")).getText();
       Assert.assertEquals(numberOfProducts,0);
       Assert.assertEquals(warning,"Your shopping cart is empty.");
    }




}
