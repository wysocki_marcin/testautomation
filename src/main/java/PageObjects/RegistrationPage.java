package PageObjects;

import Utils.Utils;
import Utils.UserData;
import Utils.UserDataEnum;
import org.openqa.selenium.By;

import java.util.HashMap;

public class RegistrationPage {

    HashMap<UserDataEnum, String> userData;
    public RegistrationPage() {
        userData = UserData.getUserData();
    }

    public void fillUpRegistrationForm() {
        selectGender();
        typeFirstName();
        typeLastName();
        typePassword();
        selectBirthDay();
        selectBirthMonth();
        selectBirthYear();
        typeCompany();
        typeAddress();
        typeCity();
        selectState();
        typePostalCode();
        selectCountry();
        typeHomePhone();
        typeMobilePhone();
    }

    private void selectGender() {
        String genderID = userData.get(UserDataEnum.GENDER).equals("Mr") ?
                "uniform-id_gender1" : "uniform-id_gender2";
        Utils.waitAndFindElement(By.id(genderID)).click();
    }

    private void typeFirstName() {
        Utils.waitAndFindElement(By.id("customer_firstname")).sendKeys(userData.get(UserDataEnum.NAME));
    }
    private void typeLastName() {
        Utils.waitAndFindElement(By.id("customer_lastname")).sendKeys(userData.get(UserDataEnum.LASTNAME));
    }
    private void typePassword() {
        Utils.waitAndFindElement(By.id("passwd")).sendKeys(userData.get(UserDataEnum.PASSWORD));
    }

    private void selectFromMenu(String dateType, UserDataEnum data) {
        StringBuilder id = new StringBuilder();
        id.append("//*[@id=\"");
        id.append(dateType);
        id.append("\"]/option[@value='");
        id.append(userData.get(data));
        id.append("']");
        Utils.waitAndFindElement(By.xpath(id.toString())).click();
    }
    private void selectBirthDay() {
        selectFromMenu("days", UserDataEnum.BIRTHDAY);
    }
    private void selectBirthMonth() {
        selectFromMenu("months", UserDataEnum.BIRTHMONTH);
    }
    private void selectBirthYear() {
        selectFromMenu("years", UserDataEnum.BIRTHYEAR);
    }
    private void typeCompany() {
        Utils.waitAndFindElement(By.id("company")).sendKeys(userData.get(UserDataEnum.COMPANY));
    }
    private void typeAddress() {
        Utils.waitAndFindElement(By.id("address1")).sendKeys(userData.get(UserDataEnum.ADDRESS));
    }
    private void typeCity() {
        Utils.waitAndFindElement(By.id("city")).sendKeys(userData.get(UserDataEnum.CITY));
    }
    private void selectState() {
        selectFromMenu("id_state", UserDataEnum.STATE);
    }
    private void typePostalCode() {
        Utils.waitAndFindElement(By.id("postcode")).sendKeys(userData.get(UserDataEnum.POSTALCODE));
    }
    private void selectCountry() {
        selectFromMenu("id_country", UserDataEnum.COUNTRY);
    }
    private void typeHomePhone() {
        Utils.waitAndFindElement(By.id("phone")).sendKeys(userData.get(UserDataEnum.HOMEPHONE));
    }
    private void typeMobilePhone() {
        Utils.waitAndFindElement(By.id("phone_mobile")).sendKeys(userData.get(UserDataEnum.MOBILEPHONE));
    }

    public void clickRegister() {
        Utils.waitAndFindElement(By.id("submitAccount")).click();
    }
}
