package PageObjects;

import Utils.UserData;
import Utils.UserDataEnum;
import Utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Header {
    public void goToLoginPage() {
        Utils.getDriver().findElement(By.cssSelector(".login")).click();
    }

    public void validateUserIsLoggedIn() {
        WebElement signOutbutton = Utils.getDriver().findElement(By.cssSelector(".logout"));
        Assert.assertTrue(signOutbutton.isDisplayed());
        WebElement header_user_info = Utils.getDriver().findElement(By.cssSelector(".header_user_info"));
        Assert.assertTrue(header_user_info.isDisplayed());
        Assert.assertEquals(header_user_info.getText(),getExpectedUserInfo());
    }

    private String getExpectedUserInfo() {
        StringBuilder userInfo = new StringBuilder();
        String name = UserData.getUserData().get(UserDataEnum.NAME);
        String lastName = UserData.getUserData().get(UserDataEnum.LASTNAME);
        userInfo.append(name);
        userInfo.append(" ");
        userInfo.append(lastName);
        return userInfo.toString();
    }
}
