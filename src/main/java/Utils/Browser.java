package Utils;

public class Browser {

    public void open(String address) {
        Utils.getDriver().navigate().to(address);
        Utils.getDriver().manage().window().maximize();
    }
}
