package Utils;

import java.util.HashMap;
public class UserData {
    private static HashMap<UserDataEnum, String> userData;
    private static void createUserData() {
            userData = new HashMap<UserDataEnum, String>();
            userData.put(UserDataEnum.GENDER, "Mr");
            userData.put(UserDataEnum.NAME, "TestName");
            userData.put(UserDataEnum.LASTNAME, "TestLastName");
            userData.put(UserDataEnum.PASSWORD, "TestTest2%");
            userData.put(UserDataEnum.BIRTHDAY, "23");
            userData.put(UserDataEnum.BIRTHMONTH, "10");
            userData.put(UserDataEnum.BIRTHYEAR, "1991");
            userData.put(UserDataEnum.COMPANY, "testCompany");
            userData.put(UserDataEnum.ADDRESS, "testAddress");
            userData.put(UserDataEnum.CITY, "testCity");
            userData.put(UserDataEnum.STATE, "1");
            userData.put(UserDataEnum.POSTALCODE, "00000");
            userData.put(UserDataEnum.COUNTRY, "21");
            userData.put(UserDataEnum.HOMEPHONE, "11111111");
            userData.put(UserDataEnum.MOBILEPHONE, "222222222");
            userData.put(UserDataEnum.DEFAULTEMAIL, "test2019_09_29__11_08_50@mailinator.com");
    }

    public static HashMap<UserDataEnum, String> getUserData() {
        if (userData == null) {
            createUserData();
        }
        return userData;
    }
}
