package Utils;

public enum UserDataEnum {
    GENDER,
    NAME,
    LASTNAME,
    PASSWORD,
    BIRTHDAY,
    BIRTHMONTH,
    BIRTHYEAR,
    COMPANY,
    ADDRESS,
    CITY,
    STATE,
    POSTALCODE,
    COUNTRY,
    HOMEPHONE,
    MOBILEPHONE,
    DEFAULTEMAIL
}
