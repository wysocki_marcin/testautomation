package Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Utils {
    private static WebDriver driver = null;
    private static WebDriverWait wait = null;
    public static WebDriver getDriver() {
        if (driver == null) {
            driver = new ChromeDriver();
        }
        return driver;

    }
    public static void setWebdriver(WebDriver webdriver)
    {
        driver = webdriver;
    }

    public static WebElement waitAndFindElement(By selector){
        if (wait == null) {
            wait = new WebDriverWait(getDriver(), 5);
        }
        wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        return getDriver().findElement(selector);
    }

    public static String generateNewEmail() {
        String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
        StringBuilder email = new StringBuilder("test");
        email.append(timestamp);
        email.append("@mailinator.com");
        return email.toString();
    }


}
