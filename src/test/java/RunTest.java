import PageObjects.Header;
import PageObjects.LoginPage;
import PageObjects.MainPage;
import PageObjects.RegistrationPage;
import Utils.Browser;
import Utils.Utils;
import Utils.UserData;
import Utils.UserDataEnum;
import org.junit.*;


public class RunTest {
    private String userEmail;
    private String pass = UserData.getUserData().get(UserDataEnum.PASSWORD);
    private Browser browser;
    private final String mainPageAddress = "http://automationpractice.com/index.php";

    @BeforeClass
    public static void suiteSetup() {
        System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
    }
    @Before
    public void testSetup() {
        browser = new Browser();
        browser.open(mainPageAddress);
    }

    @After
    public void testTearDown() {
        Utils.getDriver().manage().deleteAllCookies();
    }

    @AfterClass
    public static void suiteTearDown() {
      Utils.getDriver().quit();
    }

    @Test
    public void userRegistrationTest() {
        Header header = new Header();
        header.goToLoginPage();
        LoginPage loginPage = new LoginPage();
        userEmail = Utils.generateNewEmail();
        loginPage.typeEmailIntoRegistrationForm(userEmail);
        loginPage.clickCreateAccount();
        RegistrationPage registration = new RegistrationPage();
        registration.fillUpRegistrationForm();
        registration.clickRegister();
        header.validateUserIsLoggedIn();
    }

    @Test
    public  void userAuthorizationTest() {
        if (userEmail == null) {
            userEmail = UserData.getUserData().get(UserDataEnum.DEFAULTEMAIL);
        }
        Header header = new Header();
        header.goToLoginPage();
        LoginPage loginPage = new LoginPage();
        loginPage.logToApplication(userEmail, pass);
        header.validateUserIsLoggedIn();
    }

    @Test
    public void customTest() {
        MainPage mainPage = new MainPage();
        mainPage.goToCheckout();
        mainPage.checkIfCartIsEmpty();
    }
}
