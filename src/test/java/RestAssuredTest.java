import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Map;
import static io.restassured.RestAssured.get;

public class RestAssuredTest {
    int highestUserID = -1;

    @Test
    public void highestUserID() {
        ArrayList<Map<String, ?>> jsonAsArrayList = get("https://jsonplaceholder.typicode.com/posts/").
                then().extract().jsonPath().get("");
        highestUserID = 0;
        for (Map<String, ?> object : jsonAsArrayList) {
            int newID = Integer.parseInt(object.get("userId").toString());
            if (newID > highestUserID) {
                highestUserID = newID;
            }
        }
        Assert.assertEquals(10, highestUserID);
    }

    @Test
    public void highestID() {
        highestUserID = highestUserID == -1 ? 10 : highestUserID;
        ArrayList<Map<String, ?>> jsonAsArrayList = get("https://jsonplaceholder.typicode.com/posts?userId=" + highestUserID).
                then().extract().jsonPath().get("");
        int highestID = 0;
        for (Map<String, ?> object : jsonAsArrayList) {
            int newID = Integer.parseInt(object.get("id").toString());
            if (newID > highestID) {
                highestID = newID;
            }
        }

        Assert.assertEquals(100, highestID);
    }
}
